import Knex from "knex";
import { hashPassword } from "../hash";
const knexfile = require("../knexfile"); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["test"]); // Now the connection is a testing connection.
import { UserService } from "./UserService";

describe("userService", () => {
  let userService: UserService;

  beforeEach(async () => {
    userService = new UserService(knex);
    await knex('users').where('username', 'b')
  .del()
  });

  it("should get an user", async () => {
    const user = await userService.getUsers("randy@tecky.io");
    expect(user.length).toBe(1);
  });

  it("should sign up", async () => {
    const user = await userService.signUp(
      "b@b.com",
      await hashPassword("b"),
      "b",
      22
    );
    expect(typeof user).toBe("object");
  });

//   it("should update the score", async () => {
//     await userService.updateScore("randy", 2);
    
//     expect(UserController.logout).toBeUndefined();
//     expect(UserController.signUp).toBeUndefined();
//     expect(UserController.getCurrentUserInfo).toBeUndefined();
//   });
});
