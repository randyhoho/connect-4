export class Board {
    rows; // Height
    columns; // Width
    winCondition; //number of connections to win
    round; // 1: player1, 2: player2

    board = [];
    freeRowNumberPerColumn = [];

    winningArray = [];
    
    status = 0; // 0: running, 1: player1Won, 2: player1Lost, 3: draw
    turn = 0;
    round; // 1: player1, 2: player2

    constructor(round=1, rows=6, columns=7, winCondition=4) {0
        this.rows = rows;
        this.columns = columns;
        this.winCondition = winCondition;
        this.round = round;
        
        for (let i = 0; i < rows; i++) {
            this.board.push([])
            for(let j = 0; j < columns; j++){
                this.board[i].push(0);
            }            
        }

        for (let column = 0; column < columns; column++){
            this.freeRowNumberPerColumn.push(rows - 1);
        }
    }

    update(column) {
        if (this.status != 0 || this.allCellsInColumnFilledIn(column)) {
            return;
        }
        this.turn++;

        const row = this.freeRowNumberPerColumn[column];
        
        this.board[row][column] = this.round;
        this.freeRowNumberPerColumn[column]--;

        this.updateStatus(row, column);
        
        this.nextTurn();
    }

    updateStatus(row, column){
        if (this.playerCanWin(row, column)) {
            this.status = this.round;
        }
        else if (this.isFull()) {
            this.status = 3;
        }
    }

    nextTurn() {
        if (this.round == 1) {
            this.round = 2;
        } else {
            this.round = 1;
        }
    }

    allCellsInColumnFilledIn(column) {
        let lastRowNumber = this.freeRowNumberPerColumn[column];
        return lastRowNumber == -1;
    }

    isFull() {
        for (let freeRowNumber of this.freeRowNumberPerColumn) {
            if (freeRowNumber != -1) {
                return false;
            }
        }
        return true;
    }

    playerCanWin(row, column) {
        if (this.columnHasAConnection(column)
            || this.rowHasAConnection(row)
            || this.positiveDiagonalHasAConnection(row, column)
            || this.negativeDiagonalHasAConnection(row, column)) {
            return true;
        }
        return false;
    }

    columnHasAConnection(column) {
        let consecutiveCells = 0;
        for (let row = 0; row < this.rows; row++) {
            if (this.board[row][column] == this.round) {
                if (++consecutiveCells == this.winCondition) {
                    this.winningArray = [];
                    for (let i = 0; i < 4; i++) {
                        this.winningArray.push([row - i, column]);
                    }
                    return true;
                }
            }
            else {
                consecutiveCells = 0;
            }
        }
        return false;
    }

    rowHasAConnection(row) {
        let consecutiveCells = 0;
        for (let column = 0; column < this.columns; column++) {
            if (this.board[row][column] == this.round) {
                if (++consecutiveCells == this.winCondition) {
                    this.winningArray = [];
                    for (let i = 0; i < 4; i++) {
                        this.winningArray.push([row, column - i])
                    }
                    return true;
                }
            } else {
                consecutiveCells = 0;
            }
        }
        return false;
    }

    positiveDiagonalHasAConnection(row, column) {
        let initialRow = row;
        let initialColumn = column;
        while (initialRow > 0 && initialColumn > 0) {
            initialRow--;
            initialColumn--;
        }

        let consecutiveCells = 0;
        for (let row = initialRow, column = initialColumn; row < this.rows && column < this.columns; ++row, ++column) {
            if (this.board[row][column] == this.round) {
                if (++consecutiveCells == this.winCondition) {
                    this.winningArray = [];
                    for (let i = 0; i < 4; i++) {
                        this.winningArray.push([row - i, column - i])
                    }
                    return true;
                }
            } else {
                consecutiveCells = 0;
            }
        }
        return false;
    }

    negativeDiagonalHasAConnection(row, column) {
        let initialRow = row;
        let initialColumn = column;
        while (initialRow > 0 && initialColumn < (this.board.columns - 1)) {
            initialRow--;
            initialColumn++;
        }

        let consecutiveCells = 0;
        for (let row = initialRow, column = initialColumn; row < this.rows && column < this.columns; ++row, --column) {
            if (this.board[row][column] == this.round) {
                if (++consecutiveCells == this.winCondition) {
                    this.winningArray = [];
                    for (let i = 0; i < 4; i++) {
                        this.winningArray.push([row - i, column + i])
                    }
                    return true;
                }
            } else {
                consecutiveCells = 0;
            }
        }
        return false;
    }

    print() {
        let board = ""
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.columns; j++) {
                board += this.board[i][j] + " "
            }
            board += "\n";
        }
        

        //for debugging
        if(this.status == 1){
            console.log(`Turn = ${this.turn}, Winner = player 1`);
        }
        else if(this.status == 2){
            console.log(`Turn = ${this.turn}, Winner = player 2`);
        }
        else if(this.status == 3){
            console.log(`Turn = ${this.turn}, Draw`);
        }
        else{
            console.log(`Turn = ${this.turn}, Next Player = ${this.round}`);
        }
        console.log(board);
    }

    copy() {
        let new_board = new Board(this.round, this.rows, this.columns, this.winCondition);

        for (let i = 0; i < this.rows; i++) {
            for(let j = 0; j < this.columns; j++){
                new_board.board[i][j] = this.board[i][j];
            }
        }
        for(let index in this.freeRowNumberPerColumn){
            new_board.freeRowNumberPerColumn[index] = this.freeRowNumberPerColumn[index];
        }

        new_board.status = this.status;
        new_board.turn = this.turn;
        new_board.round = this.round;

        return new_board;
    }
}