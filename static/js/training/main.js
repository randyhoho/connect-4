import { Game } from "./Game.mjs";

const status = document.querySelector('#status');
const turn = document.querySelector('#turn');
const time = document.querySelector('#time');
const statusTable = document.querySelector('#status-table');
const turnTable = document.querySelector('#turn-table');
const timeTable = document.querySelector('#time-table');
const startButton = document.querySelector('#run-btn');
const spinner = document.querySelector("#spinner");

window.onload = () => {
    addEventListener();
}

async function main() {
    let count = 0;
    let computers = [[1, 1], [1, 2], [2, 1], [2, 2]];
    let depth = 7;

    const startTime = new Date().getTime();
    let rowInsert = 1;
    let columnInsert = 0;
    for (let i = 4; i < depth; i++) {
        for (const computer of computers) {
            let statusRow = statusTable.insertRow(rowInsert);
            let turnRow = turnTable.insertRow(rowInsert);
            let timeRow = timeTable.insertRow(rowInsert);

            status.innerHTML += `
                <td>
                    A${computer[0]}B${computer[1]}D${i}
                </td>
            `;
            turn.innerHTML += `
                <td>
                    A${computer[0]}B${computer[1]}D${i}
                </td>
            `;
            time.innerHTML += `
                <td>
                    A${computer[0]}B${computer[1]}D${i}
                </td>
            `;
            let statusCell = statusRow.insertCell(columnInsert);
            statusCell.appendChild(document.createTextNode(`A${computer[0]}B${computer[1]}D${i}`));
            let turnCell = turnRow.insertCell(columnInsert);
            turnCell.appendChild(document.createTextNode(`A${computer[0]}B${computer[1]}D${i}`));
            let timeCell = timeRow.insertCell(columnInsert);
            timeCell.appendChild(document.createTextNode(`A${computer[0]}B${computer[1]}D${i}`));
            columnInsert++;
            for (let j = 4; j < depth; j++) {
                for (const computer2 of computers) {
                    let result = getComputerGameResult([computer[0], computer[1], i], [computer2[0], computer2[1], j]);
                    let statusCell = statusRow.insertCell(columnInsert);
                    statusCell.appendChild(document.createTextNode(result.status));
                    let turnCell = turnRow.insertCell(columnInsert);
                    turnCell.appendChild(document.createTextNode(result.turn));
                    let timeCell = timeRow.insertCell(columnInsert);
                    timeCell.appendChild(document.createTextNode(result.averageTimeTaken));
                    columnInsert++;
                    count++;
                }
            }

            rowInsert++;
            columnInsert = 0;
        }
    }

    const timeTaken = new Date().getTime() - startTime;

    spinner.style.visibility = "hidden";

    console.log(`GAME COUNT: ${count}, TOTAL TIME TAKEN: ${(timeTaken / 1000).toFixed(1)}s `)
}


function getComputerGameResult(computer1, computer2) {
    let computerGame = new Game();
    while (computerGame.board.status == 0) {
        computerGame.computerMovement(computer1[0], computer1[1], computer1[2]);
        computerGame.computerMovement(computer2[0], computer2[1], computer2[2]);
    }
    console.log(computerGame.getResult())
    return computerGame.getResult();
}

function addEventListener() {
    startButton.onclick = () => {
        spinner.style.visibility = "visible";
        setTimeout(main, 500);        
    }
}