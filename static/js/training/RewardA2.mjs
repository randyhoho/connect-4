export class RewardA2 {
    vertical_points = 0;
    horizontal_points = 0;
    positiveDiagonal_points = 0;
    negativeDiagonal_points = 0;
    
    computer;
    opponent;
    board = [];
    winScore = 100000;

    constructor(round, board) {
        this.computer = round;
        if(round == 1){
            this.opponent = 2;
        }
        else {
            this.opponent = 1;
        }
        this.board = board;
    }

    getReward() {
        // Check each column for vertical score
        for (let row = 0; row < this.board.rows - this.board.winCondition + 1; row++) {
            for (let column = 0; column < this.board.columns; column++) {
                const score = this.getPositionReward(row, column, 1, 0);
                if (score == this.winScore) return this.winScore;
                if (score == -this.winScore) return -this.winScore;
                this.vertical_points += score;
            }
        }

        // Check each row's score for Horizontal points
        for (let row = 0; row < this.board.rows; row++) {
            for (let column = 0; column < this.board.columns - this.board.winCondition + 1; column++) {
                const score = this.getPositionReward(row, column, 0, 1);
                if (score == this.winScore) return this.winScore;
                if (score == -this.winScore) return -this.winScore;
                this.horizontal_points += score;
            }
        }

        // Check each positive diagonal's score
        for (let row = 0; row < this.board.rows - this.board.winCondition + 1; row++) {
            for (let column = 0; column < this.board.columns - this.board.winCondition + 1; column++) {
                const score = this.getPositionReward(row, column, 1, 1);
                if (score == this.winScore) return this.winScore;
                if (score == -this.winScore) return -this.winScore;
                this.positiveDiagonal_points += score;
            }
        }

        // Check each negative diagonal's score
        for (let row = this.board.winCondition - 1; row < this.board.rows; row++) {
            for (let column = 0; column <= this.board.columns - this.board.winCondition; column++) {
                const score = this.getPositionReward(row, column, -1, 1);
                if (score == this.winScore) return this.winScore;
                if (score == -this.winScore) return -this.winScore;
                this.negativeDiagonal_points += score;
            }
        }

        return this.horizontal_points + this.vertical_points + this.positiveDiagonal_points + this.negativeDiagonal_points;
    }

    getPositionReward(row, column, delta_y, delta_x) {
        let human_points = 0;
        let computer_points = 0;

        // Determine score through amount of available chips
        for (let i = 0; i < this.board.winCondition; i++) {
            if (this.board.board[row][column] == this.opponent) {
                human_points++; // Add for each human chip
            } else if (this.board.board[row][column] == this.computer) {
                computer_points++; // Add for each computer chip
            }

            // Moving through our board
            row += delta_y;
            column += delta_x;
        }

        if (human_points == this.board.winCondition) {
            return -this.winScore;
        } else if (computer_points == this.board.winCondition) {
            return this.winScore;
        } else {
            // Return normal points
            return computer_points;
        }
    }
}