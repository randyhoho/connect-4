# Connect4 minimax algorithm

## Getting started
Open training.html and get game result.
- [reference](https://ww2.mathworks.cn/help/reinforcement-learning/ug/what-is-reinforcement-learning.html)

## Policy
| Condition | 1 | 2 | 3 |
| :--- | :---: | :---: | :---: |
| speed(A) | consider game speed | consider generate speed | - |
| first step input(B) | must be middle column | must not be middle column | - |
| depth | 4 | 5 | 6 |
