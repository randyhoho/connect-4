import { Board } from './Board.mjs'
import { MinimaxA1B1 } from './MinimaxA1B1.mjs';
import { MinimaxA1B2 } from './MinimaxA1B2.mjs';
import { MinimaxA2B1 } from './MinimaxA2B1.mjs';
import { MinimaxA2B2 } from './MinimaxA2B2.mjs';

export class Game {
    timeTaken = 0;
    board;

    constructor(round = 1, rows = 6, columns = 7, winCondition = 4) {
        this.board = new Board(round, rows, columns, winCondition);
    }

    computerMovement(a, b, depth) {
        if (this.board.status != 0) {
            return;
        }
        if (depth > 0) {
            // Record AI decision time taken
            const startTime = new Date().getTime();

            // Algorithm call
            let minimax;
            if(a == 1 && b == 1){
                minimax = new MinimaxA1B1(this.board.round);
            }
            else if(a == 1 && b == 2){
                minimax = new MinimaxA1B2(this.board.round);
            }
            else if(a == 2 && b == 1){
                minimax = new MinimaxA2B1(this.board.round);
            }
            else if(a == 2 && b == 2){
                minimax = new MinimaxA2B2(this.board.round);
            }
            const ai_move = minimax.maximizePlay(this.board, depth);

            // Print AI decision time taken
            const timeTaken = new Date().getTime() - startTime;
            if(this.board.round % 2 != 0){
                this.timeTaken += timeTaken;
            }

            // Place ai decision
            this.board.update(ai_move[0]);
        }
    }

    getResult(){
        const averageTimeTaken = this.timeTaken / Math.ceil(this.board.turn/2);
        const result = {
            status: this.board.status,
            turn: this.board.turn,
            timeTaken: (this.timeTaken/1000).toFixed(3) + "s",
            averageTimeTaken: averageTimeTaken.toFixed(3) + "ms"
        }
        return result;
    }

    restartGame(round = this.board.round) {
        let new_board = new Board(round, this.board.rows, this.board.columns, this.board.winCondition);
        this.board = new_board;
    }
}