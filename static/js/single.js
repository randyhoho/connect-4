const playerInputElement = document.querySelector('#player1-input');
const playerInputLabel = document.getElementById('label-player1-input');
const colorSelectElement = document.querySelector('#color-select');
const difficultSelectElement = document.getElementById('difficult-select');

window.onload = async() =>{
    const res = await fetch('/getCurrentUserInfo');
    const result = await res.json();
    const username = result.username;
    if(username){
        playerInputLabel.innerHTML = username;
        playerInputElement.setAttribute("placeholder", username);
    }
}

colorSelectElement.addEventListener('change', (event) => {
    if(event.target.value == 0){
        playerInputElement.style.backgroundColor = "rgb(255, 150, 150)";
    }
    else {
        playerInputElement.style.backgroundColor = "rgb(140, 140, 255)";
    }
});

document.querySelector('#single-form').onsubmit = async function(event){
    event.preventDefault();

    const form = event.target;
    
    const formObj = {
        player_name: form.player_name.value,
        color: colorSelectElement.value,
        difficulty: difficultSelectElement.value
    }

    let res = await fetch('/single_game',{
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj)
    });

    if(res.status === 200){
        window.location = './singlegame.html';
    }
}