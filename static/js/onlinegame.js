const player1Layer = document.querySelector("#player1");
const player2Layer = document.querySelector("#player2");
const player1NameDiv = document.querySelector("#player1-name");
const player2NameDiv = document.querySelector("#player2-name");
const player1Circle = document.querySelector("#player1-circle");
const player2Circle = document.querySelector("#player2-circle");
const player1WinIcon = document.querySelector("#player1-win");
const player2WinIcon = document.querySelector("#player2-win");
const btnNewGame = document.getElementById("btn-new-game");
const btnExit = document.getElementById("btn-exit");
const circleLayers = document.querySelectorAll(".circle");
const gameColumns = document.querySelectorAll(".game-columns");
const gameMessageLayer = document.getElementById("game-message");

let socket = io.connect()
let username;
let userId;
let points;


const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const roomId = urlParams.get('id')
console.log(roomId);




//playerSetting
let player1Name = "Player 1";
let player2Name = "Player 2";
let playerIndex;



//gameBoard
let NUMBER_OF_ROWS = 6;
let NUMBER_OF_COLUMNS = 7;
let NUMBER_OF_CONNECTIONS_TO_WIN = 4;

//gameState
let gameStarted;
let gameFinished;
let lastFreeRowPerColumn;
let winner;
let playerNumber;
let board;

//animation
let animation = anime({
    loop: false,
    autoplay: false,
    targets: '#game-message',
    opacity: [0, 1],
    easing: "easeInOutQuad",
    duration: 1500,
});

window.onload = async () => {
    await gameSocket();
    await newGame();
    addEventListener();
}



async function gameSocket() {
    let res = await fetch('/getCurrentUserInfo');
    let userInfo = await res.json();
    username = userInfo.username;
    userId = userInfo.userId;
    points = userInfo.points;
    await socket.emit("join_room", roomId, username)
    socket.on('game_data', async (roomData) => {
        player1Name = await roomData.Player1;
        player2Name = await roomData.Player2;
        await setPlayerName();
        if (player1Name && player2Name != "waiting") {
            setGameMessage(`Game start!`);
        } else {
            setGameMessage(`Waiting for another player...`);
        }
    })

    btnExit.addEventListener('click', async () => {
        await socket.emit("exit_game", username);
        window.location = '/matching.html'
        console.log(`${username} left`)
    })

    socket.on('opponent_leave',()=>{
        initGame();
        setGameMessage("player has left, You Won, Redirecting to matching room...")
        setTimeout(() => {
            window.location = '/matching.html'
        }, 5000);
    })

    socket.on('update_roomData', (newData) => {
        console.log(newData)
        player1Name = newData.Player1;
        player2Name = newData.Player2;
        setPlayerName();
    })

         
    // socket.on('host_left',()=>{
    //     setGameMessage("Host has left, redirect to matching room...")
    //     setTimeout(() => {
    //         window.location = '/matching.html';
    //     }, 3000)})
}


async function newGame() {
    initGame();
    await initGui();
}

async function initGui() {
    initGuiBoard();
    setGameMessage("");
    setGameMessage(`${player1Name}'s turn`);
    player1Circle.style.visibility = "visible";
    player2Circle.style.visibility = "hidden";
    player1WinIcon.style.visibility = "hidden";
    player2WinIcon.style.visibility = "hidden";
    player1Layer.style.animation = "0.8s linear 0s infinite running changeOpacity";
    player2Layer.style.animation = "none";
    player2Layer.style.opacity = "0.6";
    gameStarted = false;
}

async function initGuiBoard() {
    console.log(username)
    await socket.on('player_index', (player1, player2) => {
        if (username == player1) {
            playerIndex = 0;
        } else if (username == player2) {
            playerIndex = 1;
        } else if (username != player1 && player2){
            playerIndex = 100;
            console.log(username)
        }

        console.log(playerIndex)
        for (let i = 0; i < getNumberOfRows(); ++i) {
            for (let j = 0; j < getNumberOfColumns(); ++j) {
                let cell = document.getElementById("cell" + i + j);
                cell.className = cell.className.replace(/player[12]/, "");
                cell.style.border = "3px solid plum";
                
                if(player1 && player2 != "waiting"){
                gameStarted = true;

                if (playerIndex == 0) {
                    cell.disabled = false;
                    cell.onclick = function () {
                        PlayerMovement(this);
                    }
                } else if(playerIndex == 1){
                    cell.disabled = true;
                } else if(playerIndex == 100){
                    cell.disabled = true;
                }
            }
            }
        }
    })
}

function setGameMessage(msg) {
    gameMessageLayer.style.opacity = 0;
    gameMessageLayer.textContent = msg;
    animation.restart();
}


socket.on('opponent_move', (columnNumber) => {
    //receive socket
    //await response async function OpponentMovement
    drawCheckerInBoard(columnNumber);
    console.log("opponent")

    if (lastFreeRowPerColumn[columnNumber] != -1) {
        let cellToWhitesmoke = document.querySelector(`#cell${lastFreeRowPerColumn[columnNumber]}${columnNumber}`);
        cellToWhitesmoke.style.background = "whitesmoke";
        cellToWhitesmoke.style.animation = "none";
    }

    if(playerIndex == 100){
        return;}

    for (let columnNumber in lastFreeRowPerColumn) {
        if (lastFreeRowPerColumn[columnNumber] != -1) {
            for (let i = 0; i < 6; i++) {
                const cellToEnabled = document.getElementById(`cell${i}${columnNumber}`);
                cellToEnabled.disabled = false;
                cellToEnabled.onclick = function () {
                    PlayerMovement(this);
                }
            }
        }
    }
})



function PlayerMovement(cell) {
    console.log("player")
    console.log(playerIndex)
    let cellNumber = cell.id.substring(4, 6);
    let columnNumber = cellNumber[1];
    drawCheckerInBoard(columnNumber);
    //send socket columnNumber -> call playeronemovement
    socket.emit('player_move', columnNumber)
    console.log(playerIndex)

    for (let i = 0; i < getNumberOfRows(); ++i) {
        for (let j = 0; j < getNumberOfColumns(); ++j) {
            let cellToDisabled = document.getElementById("cell" + i + j);
            cellToDisabled.disabled = true;
        }
    }
    if (lastFreeRowPerColumn[columnNumber] != -1) {
        let cellToWhitesmoke = document.querySelector(`#${cell.id.substring(0, 4)}${lastFreeRowPerColumn[columnNumber]}${columnNumber}`);
        cellToWhitesmoke.style.background = "whitesmoke";
        cellToWhitesmoke.style.animation = "none";
    }

}

function drawCheckerInBoard(columnNumber) {
    if (!hasEnded() && !allCellsInColumnFilledIn(columnNumber)) {
        let lastRowNumber = getLastFreeRow(columnNumber);
        let cellToColor = document.getElementById("cell" + lastRowNumber + "" + columnNumber);
        let playerNumber = getPlayerNumber();

        if (playerNumber == 1) {
            cellToColor.style.background = "red";
            cellToColor.style.animation = "none";
            player1Circle.style.visibility = "hidden";
            player2Circle.style.visibility = "visible";
            player1Layer.style.animation = "none";
            player1Layer.style.opacity = "0.6";
            player2Layer.style.animation = "0.8s linear 0s infinite running changeOpacity";
        } else {
            cellToColor.style.background = "blue";
            cellToColor.style.animation = "none";
            player1Circle.style.visibility = "visible";
            player2Circle.style.visibility = "hidden";
            player1Layer.style.animation = "0.8s linear 0s infinite running changeOpacity";
            player2Layer.style.animation = "none";
            player2Layer.style.opacity = "0.6";
        }

        playChecker(columnNumber, getPlayerNumber());
        if (isBoardFullyFilledIn()) {
            setGameMessage("Draw")
            player1Circle.style.visibility = "hidden";
            player2Circle.style.visibility = "hidden";
            player1Layer.style.animation = "none";
            player1Layer.style.opacity = "1";
            player2Layer.style.animation = "none";
            player2Layer.style.opacity = "1";
            return;
        } else if (hasEnded()) {
            if (getWinner() == 1) {
                setGameMessage(player1Name + " won! <br> back to matching room in 10sec...")
            } else {
                setGameMessage(player2Name + " won! <br> back to matching room in 10sec...")
            }

            player1Circle.style.visibility = "hidden";
            player2Circle.style.visibility = "hidden";
            for (let i = 0; i < getNumberOfRows(); ++i) {
                for (let j = 0; j < getNumberOfColumns(); ++j) {
                    let cell = document.getElementById("cell" + i + j);
                    cell.disabled = true;
                }
            }
            socket.emit("game_ended",roomId)
            setTimeout(() => {
                window.location = '/matching.html'
            }, 5000);
            return;
        } else if (lastRowNumber - 1 >= 0) {
            let cellToOppsiteColor = document.getElementById(`cell${lastRowNumber - 1}${columnNumber}`);
            if (playerNumber == 1) {
                cellToOppsiteColor.style.background = "rgb(160, 160, 255)";
            } else {
                cellToOppsiteColor.style.background = "rgb(255, 170, 170)";
            }
            cellToOppsiteColor.style.animation = "0.8s linear 0s infinite running changeOpacity";
        }
        let nextPlayer = getPlayerNumber() + 1 == 2 ? 2 : 1;
        setPlayerNumber(nextPlayer);
        if (nextPlayer == 1) {
            setGameMessage(player1Name + "'s turn");
        } else {
            setGameMessage(player2Name + "'s turn");
        }
    }
}

function getWinner() {
    return winner;
}

function getPlayerNumber() {
    return playerNumber;
}

function setPlayerNumber(number) {
    playerNumber = number;
}

function hasEnded() {
    return gameFinished;
}

function getLastFreeRow(columnNumber) {
    return lastFreeRowPerColumn[columnNumber];
}

function getNumberOfRows() {
    return NUMBER_OF_ROWS;
}

function getNumberOfColumns() {
    return NUMBER_OF_COLUMNS;
}

function allCellsInColumnFilledIn(columnNumber) {
    let lastRowNumber = lastFreeRowPerColumn[columnNumber];
    return lastRowNumber < 0;
}

//check current board status
function playChecker(columnNumber, playerNumber) {
    if (!gameFinished && !isBoardFullyFilledIn()) {

        let row = lastFreeRowPerColumn[columnNumber];
        board[row][columnNumber] = playerNumber;

        if (playerCanWin(board, row, columnNumber, playerNumber)) {
            winner = playerNumber;
            player1Layer.style.animation = "none";
            player2Layer.style.animation = "none";
            if (winner == 1) {
                player1Layer.style.opacity = "1";
                player2Layer.style.opacity = "0.6";
                player1WinIcon.style.visibility = "visible";
            } else {
                player2Layer.style.opacity = "1";
                player1Layer.style.opacity = "0.6";
                player2WinIcon.style.visibility = "visible";
            }
            gameFinished = true;
        }

        --lastFreeRowPerColumn[columnNumber];
        if (lastFreeRowPerColumn[columnNumber] == -1) {
            for (let i = 0; i < 6; i++) {
                const cellToDisabled = document.getElementById(`cell${i}${columnNumber}`);
                cellToDisabled.disabled = true;
            }
        }

        // prints the board, used for debugging
        let str = ""
        for (let i = 0; i < NUMBER_OF_ROWS; ++i) {
            for (let j = 0; j < NUMBER_OF_COLUMNS; ++j) {
                str += board[i][j] + " "
            }
            str += "\n";
        }
        console.log(str);
    }
}

function isBoardFullyFilledIn() {
    for (let i = 0; i < lastFreeRowPerColumn.length; ++i) {
        if (lastFreeRowPerColumn[i] != -1) {
            return false;
        }
    }
    return true;
}

function initGame() {
    socket.emit('init_game', roomId);
    for (let circleLayer of circleLayers) {
        circleLayer.style.background = "whitesmoke";
    }

    board = [
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
    ];

    gameFinished = false;

    lastFreeRowPerColumn = [
        NUMBER_OF_ROWS - 1,
        NUMBER_OF_ROWS - 1,
        NUMBER_OF_ROWS - 1,
        NUMBER_OF_ROWS - 1,
        NUMBER_OF_ROWS - 1,
        NUMBER_OF_ROWS - 1,
        NUMBER_OF_ROWS - 1
    ]

    winner = -1;
    playerNumber = 1;
}

function playerCanWin(board, row, column, playerNumber) {
    if (columnHasAConnection(board, column, playerNumber) ||
        rowHasAConnection(board, row, playerNumber) ||
        diagonalHasAConnection(board, row, column, playerNumber)) {
        return true;
    }
    return false;
}

function columnHasAConnection(board, column, playerNumber) {
    let consecutiveCells = 0;
    for (let row = 0; row < board.length; ++row) {
        if (board[row][column] == playerNumber) {
            if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
                for (let i = 0; i < 4; i++) {
                    const cell = document.querySelector(`#cell${row - i}${column}`);
                    cell.style.border = "5px solid black";
                }
                return true;
            }
        } else {
            consecutiveCells = 0;
        }
    }
    return false;
}

function rowHasAConnection(board, row, playerNumber) {
    let consecutiveCells = 0;
    for (let column = 0; column < board[0].length; ++column) {
        if (board[row][column] == playerNumber) {
            if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
                for (let i = 0; i < 4; i++) {
                    const cell = document.querySelector(`#cell${row}${column - i}`);
                    cell.style.border = "5px solid black";
                }
                return true;
            }
        } else {
            consecutiveCells = 0;
        }
    }
    return false;
}

function diagonalHasAConnection(board, row, column, playerNumber) {
    if (!positiveDiagonalHasAConnection(board, row, column, playerNumber)) {
        return negativeDiagonalHasAConnection(board, row, column, playerNumber);
    }
    return true;
}

function positiveDiagonalHasAConnection(board, row, column, playerNumber) {
    let initialRow = row;
    let initialColumn = column;
    while (initialRow > 0 && initialColumn > 0) {
        initialRow--;
        initialColumn--;
    }

    let consecutiveCells = 0;
    for (let i = initialRow, j = initialColumn; i < board.length && j < board[0].length; ++i, ++j) {
        if (board[i][j] == playerNumber) {
            if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
                for (let index = 0; index < 4; index++) {
                    const cell = document.querySelector(`#cell${i - index}${j - index}`);
                    cell.style.border = "5px solid black";
                }
                return true;
            }
        } else {
            consecutiveCells = 0;
        }
    }
    return false;
}

function negativeDiagonalHasAConnection(board, row, column, playerNumber) {
    let initialRow = row;
    let initialColumn = column;

    while (initialRow > 0 && initialColumn < (board[0].length - 1)) {
        initialRow--;
        initialColumn++;
    }

    let consecutiveCells = 0;
    for (let i = initialRow, j = initialColumn; i < board.length && j >= 0; ++i, --j) {
        if (board[i][j] == playerNumber) {
            if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
                for (let index = 0; index < 4; index++) {
                    const cell = document.querySelector(`#cell${i - index}${j + index}`);
                    cell.style.border = "5px solid black";
                }
                return true;
            }
        } else {
            consecutiveCells = 0;
        }
    }
    return false;
}

async function setPlayerName() {
    player1NameDiv.innerHTML = player1Name;
    player2NameDiv.innerHTML = player2Name;
}

// async function addPlayerSetting() {
//     let res = await fetch('/single_game');
//     let result = await res.json();
//     if (result.color) {
//         color = result.color;
//     }
//     if (result.difficulty) {
//         difficulty = result.difficulty;
//     }

//     if (color == 0) {
//         if (result.playerName) {
//             player1Name = result.playerName;
//         }
//         player1NameDiv.innerHTML = player1Name;
//         player2NameDiv.innerHTML = "Computer";
//     } else {
//         player1Name = "Computer"
//         if (result.playerName) {
//             player2Name = result.playerName;
//         }
//         console.log(player1Name)
//         player1NameDiv.innerHTML = "Computer";
//         player2NameDiv.innerHTML = player2Name;
//     }
// }

function addEventListener() {
    // btnNewGame.onclick = () => {
    //     newGame();
    // }
    for (let circleLayer of circleLayers) {
        circleLayer.onmouseenter = (event) => {
            let index = event.target.id.substring(5);
            if (lastFreeRowPerColumn[index] == -1) {
                return;
            }
            let cell = document.querySelector(`#${event.target.id.substring(0, 4)}${lastFreeRowPerColumn[index]}${index}`);
            if (playerNumber == 1) {
                cell.style.background = "rgb(255, 170, 170)";
            } else {
                cell.style.background = "rgb(160, 160, 255)";
            }
            cell.style.animation = "0.8s linear 0s infinite running changeOpacity";
        }
        circleLayer.onmouseleave = (event) => {
            let index = event.target.id.substring(5);
            if (lastFreeRowPerColumn[event.target.id.substring(5)] == -1) {
                return;
            }
            let cell = document.querySelector(`#${event.target.id.substring(0, 4)}${lastFreeRowPerColumn[index]}${index}`);
            cell.style.background = "whitesmoke";
            cell.style.animation = "none";
        }
    }

}