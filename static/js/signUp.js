

document.querySelector('#signup-form').onsubmit = async function (event) {
    const form = event.target;
    event.preventDefault();

    const formObj = {
        email: form.email.value,
        password: form.password.value,
        confirm_password: form.confirm_password.value,
        username: form.username.value,
    }
    const res = await fetch('/signUp', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(formObj),
        credentials: 'include'
    });
    const result = await res.json();
    console.log(result);

    const alert = document.querySelector('#alert-container');
    alert.innerHTML = '';

    if (res.status === 200) {
        alert.innerHTML = ''
        document.querySelector('#successArea').innerHTML =
            `<div class="alert alert-success" role="alert">
                  ${result.msg}
            </div>`;
        setTimeout(() => {
            window.location = '/';
        }, 2000);
    } else {
        alert.innerHTML = `
            <div class="alert alert-danger" role="alert">
                ${result.msg}  
            </div>
         `;
    }
}