document.querySelector('#local-form').onsubmit = async function(event){
    event.preventDefault();

    const form = event.target;

    let player1_name = form.player1_name.value;
    let player2_name = form.player2_name.value;
    if(!form.player1_name.value){
        player1_name = "Player 1";
    }
    if(!form.player2_name.value){
        player2_name = "Player 2";
    }
    
    const formObj = {
        player1_name: player1_name,
        player2_name: player2_name
    }

    const res = await fetch('/local_name',{
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj)
    });
    console.log(res.status)
    if(res.status === 200){
        window.location = './localgame.html';
    }
}