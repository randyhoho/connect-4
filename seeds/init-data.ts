import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    await knex("users").insert([
        { 
            email: 'lyn@tecky.io',
            password: '$2a$12$IDz4RouwWC3BGLBrSYaJ.OKwOIqhHnTyJ4LmUVlYMHcsXM9LJ2r16',
            username: 'lyn',
            points: 100,
        },
        { 
            email: 'james@tecky.io',
            password: '$2a$12$i1GXmuUYmkOZiFVFwiymwOkFkg7DYUOq8HvrALffpDFk.Ub1LQVsC',
            username: 'james',
            points: 50,
        },
        { 
            email: 'randy@tecky.io',
            password: '$2a$12$B4B4g9hPGF0otHfVOd7S.eD5AB6IBOGmq/rimC6.QXU3dhMUlELES',
            username: 'randy',
            points: 50,
        },
    ]);
};
