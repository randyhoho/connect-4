import express from 'express';
import expressSession from 'express-session';
import { Client } from 'pg';
import dotenv from 'dotenv';
import { isLoggedIn } from './guard';
import Knex from 'knex';
import { UserController } from './controllers/UserController';
import { UserService } from './services/UserService';
import { SingleGameController } from './controllers/SingleGameController';
import { LocalGameController } from './controllers/LocalGameController';
import { Server as SocketIO } from 'socket.io';
import http from 'http';
import { Room } from './models';

const knexConfigs = require('./knexfile');
const environment = process.env.NODE_ENV || 'development';
const knexConfig = knexConfigs[environment];
const knex = Knex(knexConfig);
const app = express();
dotenv.config();


export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

client.connect();

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

app.use(expressSession({
    secret: "Example",
    resave: true,
    saveUninitialized: true
}))

export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const singleGameController = new SingleGameController();
export const localGameController = new LocalGameController();

import { routes } from './routes';



app.use('/', routes);
app.use('/static', express.static(__dirname + '/static'));
app.use(express.static('public'));
app.use(isLoggedIn, express.static('protected'));


const server = http.createServer(app);
const io = new SocketIO(server);
const {
    observerJoin,
    getCurrentObserver,
    observerLeave,
    getRoomObservers,
    formatMessage,
} = require('./utils/observers');

observerJoin()
getCurrentObserver()
observerLeave()
getRoomObservers()
//online game mode
let users: any[] = [];
let rooms: Room[] = [];
const botName = 'Connect4 everBot';

io.on('connection', (socket) => {
    console.log("connected to matching")
    const roomId = socket.id;
    //matching place logic start
    socket.on("join_server", (username, points) => {
        const user = {
            id: socket.id,
            username,
            points,
        }
        users.push(user);
        io.emit("new user", users);
        io.emit("rooms", rooms)
        console.log(rooms)
        console.log(users)
    })
    //create room logic
    socket.on("create_room", (host, color) => {
        console.log(`room ${roomId} created. `)
        let hostExist =
            rooms.filter(room => room.hostname == host)
        if (hostExist.length == 0) {
            if (color = "red") {
                rooms.push({
                    roomId: roomId,
                    hostname: host,
                    Player1: host,
                    Player2: "waiting",
                    Player1Id: socket.id,
                    Player2Id: undefined,
                });
            } else if (color = "blue") {
                rooms.push({
                    roomId: roomId,
                    hostname: host,
                    Player1: "waiting",
                    Player2: host,
                    Player1Id: undefined,
                    Player2Id: socket.id,
                });
            }
            io.emit("rooms", rooms)
            socket.emit('redirect', (roomId));
        }
        else {
            console.log("host already exist")
        }
    });


    socket.on("join_room", (roomId, username) => {
        console.log(`${username} joined ${roomId}. `)
        socket.join(roomId);
        // userService.updateScore
        let currentRoom =
            rooms.filter(room => room.roomId == roomId)
        if (username != currentRoom[0].hostname) {
            if (currentRoom[0].Player1 == "waiting") {
                currentRoom[0].Player1 = username;
                io.to(currentRoom[0].roomId).emit('newplayer', `player1 is ${username}`)
            }
            else if (currentRoom[0].Player2 == "waiting") {
                currentRoom[0].Player2 = username;
                io.to(currentRoom[0].roomId).emit('newplayer', `player2 is ${username}`)
            }
            else {
                const observer = observerJoin(socket.id, username, roomId);
                console.log(observer.username)
                io.to(currentRoom[0].roomId).emit('roomObservers', {
                    observers: getRoomObservers(observer.roomId)
                });
            }
        }


        socket.on('chatMessage', msg => {
            io.to(currentRoom[0].roomId).emit('message', formatMessage(username, msg));
        });

        io.to(currentRoom[0].roomId).emit('game_data', currentRoom[0]);
        socket.on('startBtn', function () {
            io.to(currentRoom[0].roomId).emit('game_start')
        })
        //exit room, host keep room
        socket.on('exit_room', function (username) {
            socket.leave(roomId) //useless
            if (username != currentRoom[0].hostname) {
                if (username == currentRoom[0].Player1) {
                    currentRoom[0].Player1 = "waiting";

                } else if (username == currentRoom[0].Player2) {
                    currentRoom[0].Player2 = "waiting";
                } else if (username != currentRoom[0].Player1 && currentRoom[0].Player2) {
                    const observer = observerLeave(socket.id);
                    io.to(currentRoom[0].roomId).emit('roomObservers', {
                        observers: getRoomObservers(observer.roomId)
                    });
                }
                io.to(currentRoom[0].roomId).emit('game_data', currentRoom[0]);
                io.to(currentRoom[0].roomId).emit(
                    'message',
                    formatMessage(botName, `${username} has left the chat`)
                );

            }




            else if (username == currentRoom[0].hostname) {
                console.log(`room ${roomId} has been destroyed`)
                io.to(currentRoom[0].roomId).emit('host_left')
                currentRoom = currentRoom.filter(function (ele) {
                    return ele.roomId != roomId;
                })

            }

            rooms = currentRoom;
            console.log("after exit")
            console.log(currentRoom)
        })

        socket.on('game_ended', (roomId) => {
            console.log("game ended")
            currentRoom = currentRoom.filter(function (ele) {
                return ele.roomId != roomId;
            })
            rooms = currentRoom;
        })

        //exit room logic
        socket.on('exit_game', function (username) {
            socket.leave(roomId) //useless
            if (username == currentRoom[0].Player1) {
                currentRoom[0].Player1 = "waiting";

            } else if (username == currentRoom[0].Player2) {
                currentRoom[0].Player2 = "waiting";
            }
            else if (username == currentRoom[0].hostname) {
                console.log(`room ${roomId} has been destroyed`)
            }
            //delete room whatever who left
            else if (username != currentRoom[0].Player1 && currentRoom[0].Player2) {
                return
            }
            currentRoom = currentRoom.filter(function (ele) {
                return ele.roomId != roomId;
            })
            rooms = currentRoom;
            console.log("after exit")
            console.log(currentRoom)
            io.emit('opponent_leave')

        })


        socket.on("disconnect", (reason) => {
            console.log(reason)
            console.log(username)
            console.log(socket.id)
            console.log(roomId)
        })


        //game logic      

        socket.on('init_game', (roomId, playerIndex) => {
            // io.on(roomId).on('')
            io.to(currentRoom[0].roomId).emit
                ('player_index', currentRoom[0].Player1,
                    currentRoom[0].Player2)
            socket.join(roomId)

            socket.on('player_move', (columnNumber) => {
                socket.to(roomId).emit('opponent_move', columnNumber)
            })

        })

        socket.on('new_step', (str) => {
            console.log(str)
        })
    })
})

app.use((req, res) => {
    res.redirect('/404.html');
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}`);
})