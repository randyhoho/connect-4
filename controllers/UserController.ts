import { Request, Response } from "express";
import { checkPassword } from "../hash";
import { User } from "../models";
import { UserService } from "../services/UserService";

export class UserController {
  constructor(private userService: UserService) {}

  login = async (req: Request, res: Response) => {
    deleteCookies(req, res);
    const { email, password } = req.body;
    let users: User[] = await this.userService.getUsers(email);
    const user = users[0];
    if (user && (await checkPassword(password, user.password))) {
      req.session["user"] = user;
      res.status(200).json({ success: true });
    } else {
      res
        .status(401)
        .json({ success: false, msg: "Email/Password is incorrect!" });
    }
  };

  logout = async (req: Request, res: Response) => {
    deleteCookies(req, res);
    delete req.session["user"];
    res.status(200).json({ success: true });
  };

  signUp = async (req: Request, res: Response) => {
    //signup係咪可以唔delete cookies? 如果其他都delete左。
    deleteCookies(req, res);
    const email = req.body.email;
    const password = req.body.password;
    const password2 = req.body.confirm_password;
    const username = req.body.username;
    const points = 0;

    const isEmailExist = await this.userService.getUsers(email);
    
    if (isEmailExist[0]) {
        
      res.status(400).json({ msg: "Email have been registered." });
    } else if (password2 != password) {
      res.status(401).json({ msg: "Password not match!" });
    } else if (req.body.username == "") {
      res.status(402).json({ msg: "Please input your username." });
    } else {
      await this.userService.signUp(email, password, username, points);
      let users: User[] = await this.userService.getUsers(email);
      const user = users[0];
      req.session["user"] = user;
      res
        .status(200)
        .json({ msg: "Account created successfully! Redirect to main page." });
    }
  };

  getCurrentUserInfo = async (req: Request, res: Response) => {
    if (req.session["user"]) {
      const user = req.session["user"];
      // the code below could be problematic, since variable - user is a string.
      const userInfo = {
        id: user.id,
        username: user.username,
        points: user.points,
      };
      // the code above could be problematic, since variable - user is a string.

      res.status(201).json(userInfo);
    } else {
      res.status(200);
    }
  };
  // (username:string,points:number){
  //     await this.knex('users').update({points:points}).where({username:username});
  // }
  // updateScore = async(req:Request,res:Response)=>{

  //     this.userService.updateScore(username,points);

  // }
}

function deleteCookies(req: Request, res: Response) {
  delete req.session["player1Name"];
  delete req.session["player2Name"];
}
