# Connect4

## Getting started
This app implements Minimax Algorithm with Alpha-beta pruning for AI models and socket.io for online game. More features are implemented to improve user experience.
- [ERD](https://drive.google.com/file/d/1sJ3lRMXgPNlvFg9UY79jsV54JvtwqhKn/view?usp=sharing)

## Minimax algorithm with different policy
Open training.html and get game result.
| Policy | 1 | 2 | 3 |
| :--- | :---: | :---: | :---: |
| speed(A) | consider game speed | consider generate speed | - |
| first step input(B) | must be middle column | must not be middle column | - |
| depth | 4 | 5 | 6 |

## Game mode
- Single mode: User can play with various difficulties of AI
- Local Multiplayer mode: User(s) can play the game using one device
- Online Multiplayer: User can play game in online

## Game features
- Show message of player turn and game result
- Player can restart game

## Single mode features
- Player can set game difficulty

## Online mode features
- User can host or join game room
- Player can surrender
- Other users are available to view the battle
- Chatroom

## Other features
- Local login
- AI Model Visualization

## Resource
- [Existing project: connect-4 template](https://github.com/STiago/connect-4)
- [Connect 4 with Deep Reinforcement Learning](https://codebox.net/pages/connect4)

