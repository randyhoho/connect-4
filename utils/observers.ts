import moment from 'moment';
const observers:any[] = [];
// const observer: {
//   id: string;
//   username: string;
//   roomId: string;
// }

// Join user to chat
function observerJoin(id:string, username:string, roomId:string) {
  const observer = { id, username, roomId };

  observers.push(observer);

  return observer;
}

// Get current user
function getCurrentObserver(id:string) {
  return observers.find(observer => observer.id === id);
}



// User leaves chat
function observerLeave(id:string) {
  const index = observers.findIndex(observer => observer.id === id);

  if (index !== -1) {
    return observers.splice(index, 1)[0];
  }
}

// Get room users
function getRoomObservers(roomId:string) {
  return observers.filter(observer => observer.roomId === roomId);
}



function formatMessage(username:string, text:string) {
    return {
      username,
      text,
      time: moment().format('h:mm a')
    };
  }

module.exports = {
    formatMessage,
    observerJoin,
    getCurrentObserver,
  observerLeave,
  getRoomObservers,
};



